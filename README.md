# Petfinder (kaggle competition) solution
- Link to the competition - https://www.kaggle.com/c/petfinder-pawpularity-score/overview (data can be downloaded there)
- Semestral work for NI-MVI course (FIT CTU)

## Tech stack
- Python 3.7, PyTorch 1.10.0, PyTorch Lightning, Albumentations ([github](https://github.com/albumentations-team/albumentations/))

## Run
- Repo contains 2 Jupyter notebooks that contain CNN and EfficientNet solutions.

## Notes
- A competition has a limited resources that can be used for the submission so solutions in the repo are slightly modifed (they utilzie metadata), but they couldn't be submitted to the competition.